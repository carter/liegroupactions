Setup
=====

We're gonna be using GADTs again

> {-#OPTIONS -XGADTs #-}

We need these two things because Haskell is a little flaky about instances
involving type synonyms.  See [this
article](http://stackoverflow.com/questions/2125674/what-is-the-effect-of-type-synonyms-on-instances-of-type-classes-what-does-the)
on stackoverflow.

> {-# LANGUAGE TypeSynonymInstances #-}
> {-# LANGUAGE FlexibleInstances #-}

Ok now let's get going

> module RotationGroup where

> import FixedVec
> import LieGroup

Rotation types
==============

First set up some type defs.  We represent a rotation as a matrix, and an
angular velocity as a 3-vector.

> type Component = Double  -- can change this for more precision
> type Rot = Matrix Three Three Component
> type AngVel = Vec Three Component

Class instances
===============

Let's make our rotations an instance of group:

> instance Group Rot where
>   identity = idMat :: Rot
>   inverse = transpose
>   binOp = mult

And our angular velocities a vector space:

/*> instance VectorSpace AngVel where*/
/*>   zeroElement = vec 0 :: AngVel*/
/*>   scalarMult = (*)*/
/*>   vecAdd = (+)*/

Star map (hat map)
==================

We have a mapping between skew-symmetric matrices and 3-vectors.  Going forward
is easy:

> star :: AngVel -> Rot
> star (Cons a (Cons b (Cons c Nil))) = Cons rx $ Cons ry $ Cons rz Nil
>   where rx = Cons 0 $ Cons (negate c) $ Cons b Nil
>         ry = Cons c $ Cons 0 $ Cons (negate a) Nil
>         rz = Cons (negate b) $ Cons a $ Cons 0 Nil

Adjoint representation
======================

The first thing we need is the cross product (infinitesimal generator `ad`).

> adInf :: AngVel -> AngVel -> AngVel
> adInf x y = cross x y

The following should (maybe) move to `FixedVec.lhs` as an example of a nicely
type-checked vector operation.

> cross :: Num a => Vec Three a -> Vec Three a -> Vec Three a
> cross (Cons xa (Cons xb (Cons xc Nil))) (Cons ya (Cons yb (Cons yc Nil)))
>   = Cons cpx $ Cons cpy $ Cons cpz Nil
>     where cpx = xb*yc - yb*xc
>           cpy = ya*xc - xa*yc
>           cpz = xa*yb - ya*xb

TODO: Need to get `coAd` and `coad` implemented.

Group exp and log
=================

Exponential map
---------------

We use the Rodrigues formula for exponential map

> expMap :: AngVel -> Rot
> expMap w = idMat + (fmap ((*) s) wh) + ((1 - c) * (wo - idMat))
>     where wh = star w
>           wo = vouter w w
>           theta = vnorm w
>           c = cos theta
>           s = sin theta

Log map
-------

Now let's derive a very accurate log map, with a little inspiration from
wikipedia.  What we usually do is use the formula

$$
\theta = \arccos\left(\frac{\operatorname{trace} R - 1}{2}\right)
$$

for the angle and

$$
\omega = \frac{1}{2\sin\theta}\left(\cdots\right)
$$

for the axis.  The problem is that this has problems with division by zero when
$\theta$ is small, and at $\theta\sim\pi$ the axis computation is not stable
since the off-axis terms are so small.  In the small angle case, we just use the
approximation $R \sim *\log(R) + I$ where $\xi$ is the non-normalized axis.

In the $\theta\sim\pi$ case, wikipedia offers a solution that's almost
satisfactory.  We'll follow the same general idea.  That is, we start with
Rodrigues formula, and also use that

$$
\cos\theta = \frac{1}{2}(\trace(R)-1)
$$

Then we have

$$
R = I + *\omega \sin\theta + (*\omega)^2(1-\frac{1}{2}(\trace(R)-1))
$$

Now we can also expand out the star map squared.

$$
R = I + *\omega \sin\theta + (\omega\otimes\omega-I)(1-\frac{1}{2}(\trace(R)-1))
$$

Now we solve for $\omega\otimes\omega$:

$$
\omega\otimes\omega = 2\frac{R- I -(*\omega)\sin\theta}{3-\trace(R)} + I
$$

Now recall that the diagonal elements of $\omega\otimes\omega$ are the squares
of the components of $\omega$ itself.  We'll determine those, which will let us
easily figure out their signs as well.  Notice that $*\omega$ does not
contribute to the diagonal elements.  So our procedure is to compute the
diagonal elements of

$$
B = 2\frac{R- I}{3-\trace(R)} + I
$$

This uses only diagonal elements of $R$.


/*> logMap :: Rot -> AngVel*/
