Lie Group Actions
=================

Exp, Log, Frechet mean, geodesic and polynomial regression in Lie groups and
homogeneous spaces with Riemannian metrics.
