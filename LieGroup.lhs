
Intro
=====

This module implements the generic Lie group operations.  A Lie group is a pair
of two types, one indicating the group elements and the other the Lie algebra
elements.  To instantiate, one needs to specify the group operation, as well as
the adjoint representation and momentum map.

Setup
=====

We're gonna be using GADTs again

> {-#OPTIONS -XGADTs #-}

And additionally, we'll be combining types into classes, so we need
multi-parameter type classes:

> {-#OPTIONS -XMultiParamTypeClasses #-}

Now declare name of this module

> module LieGroup where

Preliminary type classes
========================

Note that some of this crap is probably provided by the Numeric prelude, but it
seems like that is a pretty big switch at this point.  But maybe it's an option
in the future.  If it makes this theoretical distinction cleaner then I'm all
for it.

Let's start by formalizing what a group is.

> class Group g where
>   identity :: g
>   binOp :: g -> g -> g
>   inverse :: g -> g

We also need to have vector spaces and algebras.

> class VectorSpace v where
>   zeroElement :: v
>   scalarMult :: (Floating a) => a -> v -> v
>   vecAdd :: v -> v -> v

The Lie group type class
========================

> class (Group g, VectorSpace v, VectorSpace m, VectorSpace groupTangent,
>        VectorSpace groupCoTangent)
>        => LieGroup g v m groupTangent groupCoTangent where
>   ad :: g -> v -> v
>   coAd :: g -> m -> a
>   adInf :: v -> v -> v
>   coAdInf :: v -> m -> m
>   momentumMap :: g -> m -> m

Notice that we should be a bit careful since the `leftTranslate, rightTranslate,
momentumMap` use elements of the tangent and cotangent bundle, which we
represent in the same form as a
